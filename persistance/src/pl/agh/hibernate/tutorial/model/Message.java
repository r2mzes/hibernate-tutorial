package pl.agh.hibernate.tutorial.model;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="messages")
public class Message {
	private long id;
	private Thread thread;
	private String content;
	private Date date;
	private String location;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="message_id")
	public long getId() {
		return id;
	}

	public void setId(long id){
		this.id = id;
	}

	@ManyToOne
	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	@Column(name="content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name="date")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name="location")
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Message{" +
				"id=" + id +
				", threadID=" + thread.getId() +
				", content='" + content + '\'' +
				", date=" + date +
				", location='" + location + '\'' +
				'}';
	}
}
