package pl.agh.hibernate.tutorial.model;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table (name="threads")
public class Thread {

	private long id;
	private String title;
	private Date creationDate;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="thread_id")
	public long getId() {
		return id;
	}

	public void setId(long id){
		this.id = id;
	}

	@Column(name="title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="date")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "Thread{" +
				"id=" + id +
				", title='" + title + '\'' +
				", creationDate=" + creationDate +
				'}';
	}
}
