package pl.agh.hibernate.tutorial;

import org.hibernate.Session;
import pl.agh.hibernate.tutorial.dao.MessageDao;
import pl.agh.hibernate.tutorial.dao.ThreadDao;
import pl.agh.hibernate.tutorial.model.*;
import pl.agh.hibernate.tutorial.model.Thread;
import pl.agh.hibernate.tutorial.util.HibernateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class Application {

	public static void main(String[] args) {
		Application application = new Application();

		ThreadDao threadDao = new ThreadDao();
		MessageDao messageDao = new MessageDao();

		try {
			HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().begin();

			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
			String dateInString = "11-04-2014";
			Date date = sdf.parse(dateInString);

			int messageID = 1;


			//let's create threads and messages
			System.out.println("Inserrting threads and messages...\n");
			for(int i=0;i<2;i++){
				Thread thread = new Thread();
				thread.setTitle("thread" + i);
				thread.setCreationDate(date);
				threadDao.save(thread);

				for(int j=0;j<3;j++){
					Message message = new Message();
					message.setContent("message" + messageID++);
					message.setDate(date);
					message.setLocation("Cracow");
					message.setThread(thread);
					messageDao.save(message);
				}
			}

			//let's print all threads and messages, that are in database
			application.printThreadsInDatabase(threadDao);
			application.printMessagesInDatabase(messageDao);

			//let's insert another thread
			System.out.println("Inserting thread with title: thread_INSERTED...\n");
			Thread thread1 = new Thread();
			thread1.setTitle("thread_INSERTED");
			thread1.setCreationDate(date);
			threadDao.save(thread1);

			application.printThreadsInDatabase(threadDao);

			//let's update this thread
			System.out.println("Updating therad, new title: thread_UPDATED...\n");
			thread1.setTitle("thread_UPDATED");
			threadDao.saveOrUpdate(thread1);

			application.printThreadsInDatabase(threadDao);

			//let's get all mesages in thread 1
			List<Message> messageInThread1 = messageDao.getMessagesByThread(threadDao.findById(Long.valueOf(1)));
			System.out.println("\nAll messages in thread 1: ");
			for(Message message : messageInThread1){
				System.out.println("\t"+message);
			}
			System.out.println("");

			//Let's delete all messages from thread 1
			System.out.println("Deleting all messages from thread 1...\n");
			for(Message message : messageInThread1){
				messageDao.delete(message);
			}

			application.printMessagesInDatabase(messageDao);

			HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();

		} catch (Throwable e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}

		return;
	}

	private void printThreadsInDatabase(ThreadDao threadDao){
		List<Thread> allThreads = threadDao.findAll();
		System.out.println("\nAll threads in database: ");
		for(Thread thread : allThreads)
			System.out.println("\t" + thread);
		System.out.println("");
	}

	private void printMessagesInDatabase(MessageDao messageDao){
		List<Message> allMessages =  messageDao.findAll();
		System.out.println("\nAll messages in database: ");
		for(Message message : allMessages)
			System.out.println("\t" + message);
		System.out.println("");
	}
}
