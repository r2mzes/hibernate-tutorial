package pl.agh.hibernate.tutorial.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import pl.agh.hibernate.tutorial.model.Message;

import java.util.ArrayList;
import java.util.List;
import pl.agh.hibernate.tutorial.model.Thread;


public class MessageDao extends BaseDao<Message> {

	public List<Message> getMessagesByThread(Thread thread){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(entityClass);
		criteria.add(Restrictions.eq("thread", thread));
		return criteria.list() != null ? (ArrayList<Message>) criteria.list() : new ArrayList<Message>();
	}
}
