package pl.agh.hibernate.tutorial.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import pl.agh.hibernate.tutorial.util.HibernateUtil;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public class BaseDao<T> {

	protected SessionFactory sessionFactory;
	protected final Class<T> entityClass;

	public BaseDao() {
		entityClass = getEntityClass();
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	private Class<T> getEntityClass() {
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public T findById(Long id) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(entityClass);
		criteria.add(Restrictions.eq("id", id));
		return (T) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(entityClass);
		return criteria.list() != null ? (ArrayList<T>) criteria.list() : new ArrayList<T>();
	}

	public void save(T object) {
		sessionFactory.getCurrentSession().save(object);
	}

	public void saveOrUpdate(T object) {
		sessionFactory.getCurrentSession().saveOrUpdate(object);
	}

	public void delete(T object) {
		sessionFactory.getCurrentSession().delete(object);
	}

	public void deleteById(long id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(entityClass);
		criteria.add(Restrictions.eq("id", id));
		Object object = criteria.uniqueResult();
		if (object != null) {
			session.delete(object);
		}
	}

	public void update(T object) {
		sessionFactory.getCurrentSession().update(object);
	}

	public Criteria getCriteria() {
		return sessionFactory.getCurrentSession().createCriteria(entityClass);
	}

	public Criteria getCriteria(Class clazz) {
		return sessionFactory.getCurrentSession().createCriteria(clazz);
	}

}
